import Header from "../components/Header";
import PartHead from "./partials/PartHead";
import PartIntro from "./partials/PartIntro";
import PartRegister from "./partials/PartRegister";
import PartSnapshot from "./partials/PartSnapshot";
import PartSession from "./partials/PartSession";
import PartFAQ from "./partials/PartFAQ";
import Footer from "../components/Footer";

function Homepage() {
  return (
    <div className="Homepage">
      <Header />
      <PartHead />
      <PartIntro />
      <PartRegister />
      <PartSnapshot />
      <PartSession />
      <PartFAQ />
      <Footer />
    </div>
  );
}

export default Homepage;
