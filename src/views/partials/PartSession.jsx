import { Col, Row } from "react-bootstrap";
import ComponentListed from "../../components/Listed";
import Section from "../../components/Section";
import ComponentTypograph from "../../components/Typograph";

function PartSession() {
  const data = require("../../services/dummy.json");

  return (
    <Section padding="20px 0 50px" part="session">
      <Row className="justify-content-md-center">
        <Col md={12} xs={12}>
          <ComponentTypograph
            size="25px"
            weight="700"
            align="center"
            padding="20px 0"
          >
            Testing session locations
          </ComponentTypograph>
        </Col>

        <Col md={12} xs={12}>
          <ComponentTypograph align="justify" padding="5px 0 20px">
            You for 2032 testing days will be hosted across Queensland in the
            first half of 2022. Below is a list of the regions, broken down to
            cities, towns and suburbs identified as hosts for testing sessions.
            As part of the selection process athletes will need to be prepared
            to attend at least one testing day.
          </ComponentTypograph>
        </Col>
        <Col md={12} xs={12}>
          <Row>
            {data.listed_session.map((data, idx) => {
              return (
                <Col md={6} xs={12} key={idx}>
                  <ComponentListed
                    title={data.location}
                    quotes={data.details}
                  ></ComponentListed>
                </Col>
              );
            })}
          </Row>
        </Col>
      </Row>
    </Section>
  );
}

export default PartSession;
