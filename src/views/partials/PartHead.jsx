import React from "react";
import { Col, Row } from "react-bootstrap";
import Section from "../../components/Section";
import ComponentTypograph from "../../components/Typograph";
import { ReactComponent as Head } from "../../assets/imgs/head.svg";
import { ReactComponent as CopWeb } from "../../assets/imgs/cop_web.svg";
import { ReactComponent as CopMobile } from "../../assets/imgs/cop_mobile.svg";
import { ReactComponent as DownTap } from "../../assets/imgs/down.svg";
import ComponentButton from "../../components/Button";

function PartHead() {
  const i_width = window.innerWidth < 780 ? true : false;
  const [mobile, setMobile] = React.useState(i_width);

  React.useEffect(() => setMobile(localStorage.getItem("mobile")), []);

  const head = {
    height: "400px",
    width: "100%",
    marginBottom: "40px",
  };

  const cop = {
    height: "40px",
    width: "100%",
    margin: "30px auto",
  };
  const cop_monbile = {
    height: "90px",
    width: "100%",
    margin: "0 auto 20px",
  };

  const down_tap = {
    height: "20px",
    width: "100%",
    margin: "30px auto",
  };

  return (
    <Section bgcolor="#111111" part="head">
      <Row className="justify-content-md-center">
        <Col md={4} xs={12}>
          <Head style={head} />
        </Col>
        <Col md={8} xs={12} className="py-5 ">
          {mobile ? <CopMobile style={cop_monbile} /> : <CopWeb style={cop} />}
          <ComponentTypograph
            color="#fff"
            size="30px"
            weight="700"
            align="center"
            padding="5px 0 20px"
          >
            On your marks! Get set! <br /> Wait for it …
          </ComponentTypograph>

          <ComponentButton
            variant="info"
            bgcolor="#009AC6"
            align="center"
            textcolor="#fff"
            border="#009AC6"
            direct="#register"
          >
            Register your interest
          </ComponentButton>
          <DownTap style={down_tap} className="float_animated" />
        </Col>
      </Row>
    </Section>
  );
}

export default PartHead;
