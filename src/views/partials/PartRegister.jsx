import { Col, Form, Row } from "react-bootstrap";
import Section from "../../components/Section";
import ComponentTypograph from "../../components/Typograph";
import ComponentButton from "../../components/Button";

function PartRegister() {
  const _bg = require("../../assets/imgs/bg_register.png");
  const mobile = window.innerWidth < 780 ? true : false;

  return (
    <Section bgimg={_bg} bgcolor="#0099C6" part="register">
      <Row className="justify-content-md-center py-5">
        <Col md={4} xs={12}>
          <ComponentTypograph
            color="#fff"
            size="30px"
            weight="700"
            padding="5px 0 20px"
          >
            Register now!
          </ComponentTypograph>

          <ComponentTypograph color="#fff" padding="5px 0 20px">
            Register your details and create an account. When applications open
            in December, we’ll let you know.
          </ComponentTypograph>
        </Col>
        <Col md={{ span: 7, offset: 1 }} xs={12}>
          <Form className="my-2">
            <Row className="mb-3">
              <Col md={4} xs={12}>
                <ComponentTypograph
                  color="#fff"
                  padding={mobile ? "0" : "8px 0"}
                >
                  First Name :
                </ComponentTypograph>
              </Col>
              <Col md={8} xs={12}>
                <Form.Control type="text" />
              </Col>
            </Row>
            <Row className="mb-3">
              <Col md={4} xs={12}>
                <ComponentTypograph
                  color="#fff"
                  padding={mobile ? "0" : "8px 0"}
                >
                  Sure Name :
                </ComponentTypograph>
              </Col>
              <Col md={8} xs={12}>
                <Form.Control type="text" />
              </Col>
            </Row>
            <Row className="mb-3">
              <Col md={4} xs={12}>
                <ComponentTypograph
                  color="#fff"
                  padding={mobile ? "0" : "8px 0"}
                >
                  Date of Birth :
                </ComponentTypograph>
              </Col>
              <Col md={8} xs={12}>
                <Form.Control type="date" />
              </Col>
            </Row>
            <Row className="mb-3">
              <Col md={4} xs={12}>
                <ComponentTypograph
                  color="#fff"
                  padding={mobile ? "0" : "8px 0"}
                >
                  Email Address :
                </ComponentTypograph>
              </Col>
              <Col md={8} xs={12}>
                <Form.Control type="email" />
              </Col>
            </Row>
            <Row className="mb-3">
              <Col md={4} xs={12}>
                <ComponentTypograph
                  color="#fff"
                  padding={mobile ? "0" : "8px 0"}
                >
                  Mobile Number :
                </ComponentTypograph>
              </Col>
              <Col md={8} xs={12}>
                <Form.Control type="text" />
              </Col>
            </Row>

            <Row className="mb-3">
              <Col className="mb-2" md={{ span: 4, offset: 4 }} xs={12}>
                <a href="/" className="text-light">
                  <ComponentTypograph size="10px" color="#fff" padding="0">
                    Privacy policy
                  </ComponentTypograph>
                </a>
                <a href="/" className="text-light">
                  <ComponentTypograph size="10px" color="#fff" padding="0">
                    Term and Condition
                  </ComponentTypograph>
                </a>
              </Col>
              <Col className="mb-2" md={4} xs={12}>
                <ComponentButton variant="dark" type="submit" block>
                  Register now
                </ComponentButton>
              </Col>
            </Row>
          </Form>
        </Col>
      </Row>
    </Section>
  );
}

export default PartRegister;
