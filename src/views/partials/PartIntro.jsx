import { Col, Row } from "react-bootstrap";
import Section from "../../components/Section";
import ComponentTypograph from "../../components/Typograph";
import { ReactComponent as HashTag } from "../../assets/imgs/hashtag.svg";

function PartIntro() {
  return (
    <Section padding="20px 0" part="intro">
      <Row className="justify-content-md-center">
        <Col md={12} xs={12}>
          <ComponentTypograph
            size="25px"
            weight="700"
            color="#009AC6"
            align="center"
            padding="20px 0 30px"
          >
            This could be your starting line for the 2032 Olympic and Paralympic
            Games in Brisbane
          </ComponentTypograph>
        </Col>

        <Col md={6} xs={12}>
          <ComponentTypograph align="justify">
            We’re looking for talented young Queensland athletes with
            Olympic-sized dreams to register their interest in being part of the
            Queensland Academy of Sport’s Talent Identification Program—You for
            2032! This program is strictly for talent born between 1999 and 2009
            inclusive, with Para-Sports accepting those who are a little older
            (i.e. 1992 to 2009 inclusive).
          </ComponentTypograph>
        </Col>
        <Col md={6} xs={12}>
          <Row>
            <Col md={3} xs={3}>
              <HashTag />
            </Col>
            <Col md={9} xs={9}>
              <ComponentTypograph align="justify">
                <b>Caution:</b> our standards are high (because we want the
                best!) but if you think you have what it takes to win on the
                world stage pre-register now.
              </ComponentTypograph>
            </Col>
          </Row>
          <ComponentTypograph align="justify">
            Further information will become available in January 2022 when we
            launch our application portal. By pre-registering you will
            automatically be prompted when applications are open.
          </ComponentTypograph>
        </Col>

        <Col md={12} xs={12}>
          <ComponentTypograph
            size="16px"
            weight="600"
            align="center"
            padding="30px 0 20px"
          >
            This could be the first step in your Olympic or Paralympic journey.{" "}
            <span className="text-danger">
              <i>Good Luck!</i>
            </span>
          </ComponentTypograph>
        </Col>
      </Row>
    </Section>
  );
}

export default PartIntro;
