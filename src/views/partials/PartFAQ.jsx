import React from "react";
import { Col, Row } from "react-bootstrap";
import ComponentListed from "../../components/Listed";
import Section from "../../components/Section";
import ComponentTypograph from "../../components/Typograph";

function PartFAQ() {
  const data = require("../../services/dummy.json");
  const _bg = require("../../assets/imgs/bg_faq.png");

  const [showDetail, setShowDetail] = React.useState(0);

  return (
    <Section padding="20px 0 50px" bgcolor="#F0534D" bgimg={_bg} part="faq">
      <Row className="justify-content-md-center">
        <Col md={4} xs={12}>
          <ComponentTypograph
            color="#fff"
            size="30px"
            weight="700"
            padding="20px 0"
            align="center"
          >
            FAQs
          </ComponentTypograph>
        </Col>
        <Col md={12} xs={12}>
          <Row>
            {data.faq.map((data, idx) => {
              return (
                <ComponentListed
                  key={idx}
                  bgcolor="#BA3A3A66"
                  bgselected="#BA3A3ABB"
                  border="transparent"
                  textcolor="#fff"
                  title={data.name}
                  index="Q."
                  onClick={() =>
                    setShowDetail(showDetail === idx + 1 ? 0 : idx + 1)
                  }
                  detailView={showDetail === idx + 1 ? true : false}
                  pointer
                >
                  <ComponentTypograph padding="10px">
                    {data.details}
                  </ComponentTypograph>
                </ComponentListed>
              );
            })}
          </Row>
        </Col>

        <Col md={12} xs={12}>
          <ComponentTypograph padding="10px 0" color="#fff">
            Questions? <br />
            Contact the Queensland Academy of Sport via email{" "}
            <a className="text-light" href="mailto:Youfor2032@dtis.qld.gov.au">
              Youfor2032@dtis.qld.gov.au
            </a>
          </ComponentTypograph>
        </Col>
      </Row>
    </Section>
  );
}

export default PartFAQ;
