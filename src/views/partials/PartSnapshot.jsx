import { Col, Row } from "react-bootstrap";
import Section from "../../components/Section";
import Slider from "../../components/Slider";
import ComponentTypograph from "../../components/Typograph";
import { ReactComponent as Icon2032 } from "../../assets/imgs/2032.svg";

function PartSnapshot() {
  const _bg = require("../../assets/imgs/bg_snapshot.png");
  const data = require("../../services/dummy.json");

  const scrolled = {
    display: "flex",
    maxWidth: "100vw",
    maxHeight: "200px",
    overflow: "scroll",
    position: "absolute",
    bottom: "20px",
    left: "0",
    padding: "20px 50px",
  };

  return (
    <Section
      padding="20px 0 250px"
      bgcolor="#FEC003"
      bgimg={_bg}
      part="snapshot"
    >
      <Row className="justify-content-md-center">
        <Col md={12} xs={12}>
          <ComponentTypograph
            size="25px"
            weight="700"
            align="center"
            padding="20px 0"
          >
            <i>You for 2032</i>—Program snapshot
          </ComponentTypograph>
        </Col>

        <Col md={6} xs={12}>
          <ComponentTypograph align="justify">
            The Talent Identification Program—<i>You for 2032</i>—has been
            designed to identify our next generation of elite athletes. The aim
            is to develop and nurture young Queensland talents who have the
            potential to win on the world stage at the 2032 Olympic and
            Paralympic Games.
          </ComponentTypograph>
          <ComponentTypograph align="justify">
            Athletic ability coupled with a great attitude to excel are the key
            ingredients in this program. Young athletes could be currently
            competing in a sport that isn’t part of the Olympic and Paralympic
            Games, but we may identity a sport that’s perfect for their athletic
            strengths.
          </ComponentTypograph>
        </Col>
        <Col md={6} xs={12}>
          <ComponentTypograph align="justify">
            Those successful in being selected for the program will receive
            substantial training, coaching and development support from state
            and national sporting organisations as well as the Queensland
            Academy of Sport. More details about the program will become
            available to athletes who are selected.
          </ComponentTypograph>

          <ComponentTypograph align="justify">
            Once the application portal opens in January, an assessment process
            will take place, as indicated by the below diagram.
          </ComponentTypograph>
        </Col>
      </Row>

      <div style={scrolled}>
        {data.snapshot.map((field, idx) => {
          return (
            <Slider
              key={idx}
              bgcolor={field.color}
              textcolor={field.font}
              lastidx={data.snapshot.length === idx + 1 ? true : false}
            >
              {field.hasImage ? <Icon2032 /> : field.desc}
            </Slider>
          );
        })}
      </div>
    </Section>
  );
}

export default PartSnapshot;
