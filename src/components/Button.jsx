import PropTypes from "prop-types";
import { Button } from "react-bootstrap";

export default function ComponentButton({
  variant,
  children,
  onClick,
  align,
  block,
  bgcolor,
  textcolor,
  border,
  direct,
}) {
  const core_styled = {
    textAlign: align ? align : "left",
  };

  const styled = {
    borderRadius: "2px",
    width: block ? "100%" : "",
    padding: "8px 15px",
    backgroundColor: bgcolor ? bgcolor : "",
    color: textcolor ? textcolor : "",
    border: border ? border : "",
  };

  return (
    <div style={core_styled}>
      <Button
        variant={variant}
        style={styled}
        onClick={onClick}
        size="sm"
        role={direct ? "link" : "button"}
        href={direct ? direct : ""}
      >
        {children}
      </Button>
    </div>
  );
}

ComponentButton.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
    PropTypes.string,
  ]),
  bgcolor: PropTypes.string,
  block: PropTypes.bool,
  onClick: PropTypes.func,
  align: PropTypes.string,
  textcolor: PropTypes.string,
  border: PropTypes.string,
  direct: PropTypes.string,
};
