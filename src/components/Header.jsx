import React from "react";
import { Container, Navbar } from "react-bootstrap";
import ComponentButton from "./Button";
import { ReactComponent as Logo } from "../assets/imgs/logo.svg";
import { ReactComponent as Media } from "../assets/imgs/phone_tablet.svg";

export default function Header() {
  const [mobile, setMobile] = React.useState(false);

  const logo = {
    height: "100%",
    width: "200px",
  };

  const medias = {
    height: "30px",
    width: "30px",
    margin: "0 10px",
    cursor: "pointer",
  };

  const setMobileChange = (data) => {
    setMobile(data);
    localStorage.setItem("mobile", data);
  };

  return (
    <Navbar>
      <Container>
        <Navbar.Brand href="#home">
          <Logo style={logo} />
        </Navbar.Brand>
        <Navbar.Toggle />
        <Navbar.Collapse className="justify-content-end">
          <ComponentButton variant="outline-dark" direct="#register">
            Register Now
          </ComponentButton>
          <Media style={medias} onClick={() => setMobileChange(!mobile)} />
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}
