import PropTypes from "prop-types";
import { Card, Collapse } from "react-bootstrap";
import ComponentTypograph from "./Typograph";

export default function ComponentListed({
  children,
  onClick,
  bgcolor,
  border,
  title,
  quotes,
  textcolor,
  index,
  detailView,
  pointer,
  bgselected,
}) {
  const core = {
    margin: "10px 0",
  };
  const styled = {
    cursor: pointer ? "pointer" : "",
    borderRadius: "3px",
    width: "100%",
    padding: "8px 20px",
    border: border ? border : "1px solid rgba(0, 0, 0, 0.2)",
    backgroundColor: bgcolor ? (detailView ? bgselected : bgcolor) : "#ffffff",
    borderBox: "none",
    margin: "0",
  };

  const flex_box = {
    display: "flex",
    flexDirection: "row",
    justifyContent: "left",
    alignItems: "center",
  };

  const flex_index = {
    marginRight: index ? "30px" : "0",
  };

  const child = {
    backgroundColor: "#fff",
    fontSize: "13px",
    color: "#333",
  };

  return (
    <div style={core}>
      <Card style={styled} onClick={onClick}>
        <div style={flex_box}>
          <div style={flex_index}>
            {index ? (
              <ComponentTypograph
                size="14px"
                weight="500"
                padding="0"
                color={textcolor ? textcolor : "#444"}
              >
                {index}
              </ComponentTypograph>
            ) : null}
          </div>
          <div>
            <ComponentTypograph
              size="14px"
              weight="500"
              padding="0"
              color={textcolor ? textcolor : "#444"}
            >
              {title}
            </ComponentTypograph>
            <ComponentTypograph
              size="12px"
              padding="0"
              color={textcolor ? textcolor : "#666"}
            >
              {quotes}
            </ComponentTypograph>
          </div>
        </div>
      </Card>
      <Collapse in={detailView} appear={true}>
        <div style={child}>{children}</div>
      </Collapse>
    </div>
  );
}

ComponentListed.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
    PropTypes.string,
  ]),
  onClick: PropTypes.func,
  bgcolor: PropTypes.string,
  border: PropTypes.string,
  title: PropTypes.string,
  quotes: PropTypes.string,
  textcolor: PropTypes.string,
  index: PropTypes.string,
  detailView: PropTypes.bool,
  pointer: PropTypes.bool,
  bgselected: PropTypes.string,
};
