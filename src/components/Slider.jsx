import PropTypes from "prop-types";
import styled from "styled-components";

export default function Slider({ bgcolor, children, textcolor, lastidx }) {
  const Sliders = styled.div`
    border: none;
    display: flex;
    justify-content: center;
    align-items: center;
    text-align: center;
    margin: 10px 0;
    width: fit-content;

    .content {
      display: flex;
      justify-content: center;
      align-items: center;
      text-align: center;
      height: 180px;
      width: 180px;
      padding: 0 20px;
      font-size: 18px;
      font-weight: 500;
      background-color: ${() => (bgcolor ? bgcolor : "#fff")};
      color: ${() => (textcolor ? textcolor : "#000")};
    }

    .arrow {
      display: flex;
      justify-content: center;
      align-items: center;
      text-align: center;
      font-size: 35px;
      margin-left: 0 15px;
    }
  `;
  return (
    <Sliders>
      <div className="content">{children}</div>
      <div className="arrow">{lastidx ? "" : <>&#8594;</>}</div>
    </Sliders>
  );
}

Slider.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
    PropTypes.string,
  ]),
  bgcolor: PropTypes.string,
  textcolor: PropTypes.string,
  padding: PropTypes.string,
  lastidx: PropTypes.bool,
};
