import { Container } from "react-bootstrap";
import { ReactComponent as Logo } from "../assets/imgs/logo_company.svg";
import ComponentTypograph from "./Typograph";

export default function Footer() {
  const mobile = window.innerWidth < 780 ? true : false;
  const route_list = [
    { link: "/help", name: "Help" },
    { link: "/copyright", name: "Copyright" },
    { link: "/disclaimer", name: "Disclaimer" },
    { link: "/privacy", name: "Privacy" },
    { link: "/right to information", name: "Right to information" },
    { link: "/accessibility", name: "Accessibility" },
    { link: "/jobs", name: "Jobs in Queensland Goverment" },
    { link: "/taglog", name: "Taglog(Other languanges)" },
  ];

  const core = {
    width: "100%",
    padding: "10px 0",
    textAlign: mobile ? "center" : "right",
    borderBottom: "1px solid #ddd",
  };

  const logo = {
    width: "200px",
  };

  const route = {
    textAlign: mobile ? "center" : "left",
    margin: "20px 0",
  };

  const linked = {
    fontSize: "12px",
    color: "#444",
    textDecoration: "none",
  };

  return (
    <Container>
      <div style={core}>
        <Logo style={logo} />
      </div>

      <div style={route}>
        {route_list.map((data, idx) => {
          return (
            <span className="text-secondary" key={idx}>
              <a style={linked} href={data.link}>
                {data.name}
              </a>
              <span className="mx-1">
                {idx + 1 === route_list.length ? "" : "|"}
              </span>
            </span>
          );
        })}
      </div>

      <div style={route}>
        <ComponentTypograph padding="0" align={mobile ? "center" : "left"}>
          &copy; The State of Queensland 1995-2021
        </ComponentTypograph>
        <ComponentTypograph padding="0" align={mobile ? "center" : "left"}>
          Queensland Goverment
        </ComponentTypograph>
      </div>
    </Container>
  );
}
