import PropTypes from "prop-types";
import { Container } from "react-bootstrap";

export default function Section({ bgcolor, bgimg, children, padding, part }) {
  const styled = {
    backgroundColor: bgcolor ? bgcolor : "#fff",
    backgroundImage: `url('` + bgimg + `')`,
    backgroundPosition: "center",
    backgroundRepeat: "no-repeat",
    backgroundSize: "cover",
    border: "none",
    minHeight: "300px",
    padding: padding ? padding : "0px",
    position: "relative",
  };

  return (
    <div style={styled} id={part}>
      <Container>{children}</Container>
    </div>
  );
}

Section.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
    PropTypes.string,
  ]),
  bgcolor: PropTypes.string,
  bgimg: PropTypes.string,
  padding: PropTypes.string,
  part: PropTypes.string,
};
